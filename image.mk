
build:
	docker build --build-arg app=app -f Dockerfile -t $(SERVICE):$(VERSION) .

push:
	docker tag $(SERVICE):$(VERSION) registry.gitlab.com/p8155/campaignservice/$(SERVICE):$(VERSION)
	docker push registry.gitlab.com/p8155/campaignservice/$(SERVICE):$(VERSION)

build-push:	build	push
