import json

from sanic import Sanic
from sanic import json as sanic_json

from db_manager.campaign_manager import init_campaigns, _get_all_campaigns, _create_campaign, _get_campaign, _update_campaign, get_campaigns_for_bid_request
from utils.utils import get_main_logger, create_result

app = Sanic('CampaignService')

log = get_main_logger(__name__)


@app.get("/health")
async def health(request):
    return sanic_json(create_result(result_="This is the Campaign Service Running", status_code=200))


@app.get("/campaigns")
async def get_all_camp(request):
    response = _get_all_campaigns()
    return sanic_json(response, status=response.get('status_code'))


@app.post("/campaigns")
async def create_camp(request):
    payload = json.loads(request.body)
    response = _create_campaign(payload)
    return sanic_json(response, status=200)


@app.get("/campaigns/<campaign_id>")
async def get_one_camp(request, campaign_id):
    response = _get_campaign(campaign_id)
    return sanic_json(response, status=response.get('status_code'))


@app.put("/campaigns/<campaign_id>")
async def get_one_camp(request, campaign_id):
    payload = json.loads(request.body)
    response = _update_campaign(payload)
    return sanic_json(response, status=response.get('status_code'))


@app.get("/campaigns/init")
async def init_camp(request):
    response = init_campaigns()
    return sanic_json(response, status=response.get('status_code'))


@app.post("/campaigns/request")
async def get_campaigns_for_request(request):
    payload = json.loads(request.body)
    response = get_campaigns_for_bid_request(payload)
    return sanic_json(response, status=response.get('status_code'))


if __name__ == '__main__':
    app.run('0.0.0.0', port=8090)
