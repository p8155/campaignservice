import json
import random
import uuid
from typing import List

from db_manager.manager import db_connect
from models.models import Campaign, MyLocation, BidRequest
from utils.utils import get_main_logger, create_result, func_name, dmsg

l = get_main_logger('campaign_manager')


@db_connect
def init_campaigns():
    crete_locations = [
        {"location_name": "Heraklion",
         "polygon": {'type': 'Point',
                     'coordinates': [35.3387, 25.1442]}
         },  # heraklion_geo
        {"location_name": "Agios Nikolaos",
         "polygon": {'type': 'Point',
                     'coordinates': [35.1900, 25.7164]}
         },  # agios_nik_geo
        {"location_name": "Chania",
         "polygon": {'type': 'Point',
                     'coordinates': [35.5138, 24.0180]}
         },  # chania_geo
        {"location_name": "Rethymno",
         "polygon": {'type': 'Point',
                     'coordinates': [35.3656, 24.4920]}
         },  # rethimno_geo
        {"location_name": "Sitia",
         "polygon": {'type': 'Point',
                     'coordinates': [35.2087, 26.1052]}
         }]  # sitia_geo
    try:
        _locations: List[MyLocation] = []
        temp = crete_locations[random.randint(0, len(crete_locations) - 1)]  # this procedure can be run in loop to create multiple locations
        _loc = MyLocation(name=temp.get('location_name'), loc=temp.get('polygon'))
        _locations.append(_loc)
        _camp = Campaign(name=str(uuid.uuid4()), price=random.randint(10, 1000), targetedCountries=["Greece"],
                         targetedLocations=_locations).save()
        return create_result(items=[], result_=func_name() + '_SUCCESS', status_code=200)
    except Exception as e:
        l.exception(dmsg('') + e.__str__())
        return create_result(items=[], result_=func_name() + '_FAILURE', more=e.__str__(), status_code=500)


@db_connect
def _create_campaign(payload) -> dict:
    print(payload)
    try:
        result = Campaign(**payload).save()
        return create_result(items=[result.to_json()], result_=func_name() + '_SUCCESS', status_code=200)
    except Exception as e:
        l.exception(dmsg('') + e.__str__())
        return create_result(items=[], result_=func_name() + '_FAILURE', more=e.__str__(), status_code=500)


@db_connect
def _get_all_campaigns() -> dict:
    try:
        result = Campaign.objects()
        return create_result(items=[json.loads(i.to_json()) for i in result], result_=func_name() + '_SUCCESS',
                             status_code=200)
    except Exception as e:
        l.exception(dmsg('') + e.__str__())
        return create_result(items=[], result_=func_name() + '_FAILURE', more=e.__str__(), status_code=500)


@db_connect
def _get_campaign(campaign_id: str) -> dict:
    try:
        result: Campaign = Campaign.objects.get(id=campaign_id)
        return create_result(items=[json.loads(result.to_json())], result_=func_name() + '_SUCCESS', status_code=200)
    except Exception as e:
        l.exception(dmsg('') + e.__str__())
        return create_result(items=[], result_=func_name() + '_FAILURE', more=e.__str__(), status_code=500)


@db_connect
def _update_campaign(campaign_id: str, payload: dict) -> dict:
    try:
        print(payload)
        Campaign.objects.get(id=campaign_id).update(**payload)
        return create_result(items=[json.loads(Campaign.objects.get(id=campaign_id).to_json())],
                             result_=func_name() + '_SUCCESS', status_code=200)
    except Exception as e:
        l.exception(dmsg('') + e.__str__())
        return create_result(items=[], result_=func_name() + '_FAILURE', more=e.__str__(), status_code=500)


@db_connect
def _delete_campaign(campaign_id: str) -> dict:
    try:
        result: Campaign = Campaign.objects.get(id=campaign_id)
        result.delete()
        return create_result(items=[], result_=func_name() + '_SUCCESS', status_code=200)
    except Exception as e:
        l.exception(e.__str__())
        print(e.__str__())
        return create_result(items=[], result_=func_name() + '_FAILURE', more=e.__str__(), status_code=500)


@db_connect
def get_campaigns_for_bid_request(payload) -> dict:
    try:
        print("payload: ")
        print(payload)
        # bid_request = BidRequest(**payload)  # parse request as BidRequest Model
        result = Campaign.objects.filter(targetedCountries__in=[payload.get('country')],
                                         targetedLocations__loc__near=payload.get('location'))
        return create_result(items=[i.to_json() for i in result if i], result_=func_name() + '_SUCCESS', status_code=200)
    except Exception as e:
        l.exception(dmsg('') + e.__str__())
        return create_result(items=[], result_=func_name() + '_FAILURE', more=e.__str__(), status_code=500)
