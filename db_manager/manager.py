from functools import partial, wraps
from utils.utils import get_main_logger, dmsg
from mongoengine import connect
from config.config import get_config

l = get_main_logger('dm_manager')
c = get_config()


# "mongodb+srv://yannis:<password>@cluster0.qt9ch.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

class Connection:
    def __enter__(self):
        try:
            self.conn = connect(db=c.get('db_name'), username=c.get('username'),
                                password=c.get('password'),
                                host=c.get('db_url') + ':' + c.get('db_port'),
                                retryWrites=False, alias='default')
            l.debug(f'Connected to database{c.get("db_name")}')
            print(type(self.conn))
            return self.conn
        except Exception as e:
            l.exception(e.__str__())

    def __exit__(self, exc_type, exc_val, exc_tb):
        l.debug('Closing DB connection')
        self.conn.close()

    @staticmethod
    def run_command(func):
        with Connection() as conn:
            return func()


def db_connect(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        with Connection() as conn:
            return func(*args, **kwargs)

    return wrapper



