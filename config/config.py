import argparse
import logging
import configparser

config = configparser.ConfigParser()
config.read('settings.ini')  # path of your .ini file
settings = {
    "DEV": {
        "db_name": config.get("DEV", "db_name"),
        "db_url": config.get("DEV", "db_url"),
        "db_port": config.get("DEV", "db_port"),
        "username": config.get("DEV", "username"),
        "password": config.get("DEV", "password"),
    },
    "PROD": {
        "db_name": config.get("PROD", "db_name"),
        "db_url": config.get("PROD", "db_url"),
        "db_port": config.get("PROD", "db_port"),
        "username": config.get("PROD", "username"),
        "password": config.get("PROD", "password"),
    },
    "TEST": {
        "db_name": config.get("TEST", "db_name"),
        "db_url": config.get("TEST", "db_url"),
        "db_port": config.get("TEST", "db_port"),
        "username": config.get("TEST", "username"),
        "password": config.get("TEST", "password"),
    }
}


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--mode', dest='mode', help='Set the running mode (dev/prod)')
    options = parser.parse_args() if parser.parse_args() else {}

    # Check for errors i.e if the user does not specify the target IP Address
    # Quit the program if the argument is missing
    # While quitting also display an error message
    if not options.mode:
        # Code to handle if interface is not specified
        # parser.error("[-] Please specify the running mode (dev/prod), use --help for more info.")
        logging.error("[-] Please specify the running mode (dev/prod), use --help for more info.")
        # options.mode = 'DEV'
        # return options
    else:
        pass
    return options


def get_mode():
    options = get_args()
    if options.mode:
        MODE = ('DEV' if options.mode.upper() == 'DEV' else 'PROD')
    else:
        MODE = 'DEV'
    return MODE


def get_config():
    return settings[get_mode()]
